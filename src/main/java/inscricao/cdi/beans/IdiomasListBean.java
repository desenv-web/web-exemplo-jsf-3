package inscricao.cdi.beans;

import inscricao.entity.Idioma;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.model.ListDataModel;
import javax.inject.Named;

/**
 *
 * @author Wilson
 */
@Named
@ApplicationScoped
public class IdiomasListBean {

    private final List<Idioma> idiomasList = new ArrayList<>(
        Arrays.asList(
            new Idioma(1, "Inglês"),
            new Idioma(2, "Alemão"),
            new Idioma(3, "Francês")
    ));
    
    private final ListDataModel<Idioma> idiomaDataModel = new ListDataModel<>(idiomasList);

    public IdiomasListBean() {
    }

    public ListDataModel<Idioma> getIdiomaDataModel() {
        return idiomaDataModel;
    }

    public List<Idioma> getIdiomasList() {
        return idiomasList;
    }
    
    public boolean existeCodigo(int codigo) {
        return !idiomasList.stream().noneMatch(i -> i.getCodigo() == codigo);
    }
    
    public boolean existeDescricao(String descricao) {
        return !idiomasList.stream().noneMatch(i -> i.getDescricao().equalsIgnoreCase(descricao));
    }
    
    public Idioma findByCodigo(int codigo) {
        return idiomasList.get(idiomasList.indexOf(new Idioma(codigo)));
    }
    
    public void addIdioma(Idioma idioma) {
        idiomasList.add(idioma);
    }
    
    public void addIdioma(int codigo, String descricao) {
        addIdioma(new Idioma(codigo, descricao));
    }
}
